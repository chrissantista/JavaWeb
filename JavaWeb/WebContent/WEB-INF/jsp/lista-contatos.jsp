<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.Contato, jdbc.ContatoDao, java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agenda de Contatos</title>
</head>
<body>

<c:import url="/WEB-INF/jsp/cabecalho.jsp" />

<table>
<tr>
	<td>ID</td>
	<td>Nome</td>
	<td>E-mail</td>
	<td>Endere�o</td>
	<td>Data de Nascimento</td>
	<td>Excluir</td>
	<td>Alterar</td>
</tr>
<c:forEach var="contato" items="${contatos}" varStatus="linha">
<tr bgcolor="${linha.count % 2 == 1 ? 'PowderBlue' : 'White' }">
	
	<td>${contato.id}</td>
	<td>${contato.nome}</td>
	
	<td>
		<c:choose>
			<c:when test="${not empty contato.email}">
				<a href="mailto:${contato.email}">${contato.email}</a>
			</c:when>
			<c:otherwise>
				E-mail n�o informado
			</c:otherwise>
		</c:choose>
	</td>
	
	<td>${contato.endereco}</td>
	<td><fmt:formatDate value="${contato.dataNascimento.time}" pattern="dd/MM/yyyy"/></td>
	<td><a href="mvc?logica=RemoveContatoLogic&id=${contato.id}">del</a>
	<td><a href="mvc?logica=ManipulaContatoLogic&id=${contato.id}">upd</a>
	
</tr>
</c:forEach>
</table>

<a href="mvc?logica=ManipulaContatoLogic">Adicionar contato</a>

<c:import url="/WEB-INF/jsp/rodape.jsp" />

</body>
</html>