<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastro de contato</title>
<script src="js/jquery-3.1.1.js"></script>
<script src="js/jquery-ui.js"></script>
<link href="css/jquery-ui.css" rel="stylesheet">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="chr" %>
</head>
<body>

<c:import url="/WEB-INF/jsp/cabecalho.jsp" />

<form action="mvc?logica=GravaContatoLogic&id=${contato.id}" method="post">
	Nome: <input type="text" name="nome" value="${contato.nome}" required /><br>
	E-mail: <input type="text" name="email" value="${contato.email}" required /><br>
	Endere�o: <input type="text" name="endereco" value="${contato.endereco}" required /><br>
	Data de Nascimento: <chr:campoData id="dataNascimento" value="${contato.dataNascimentoString}" />
	<input type="submit" value="Gravar" />
</form>

<c:import url="/WEB-INF/jsp/rodape.jsp" />

</body>
</html>