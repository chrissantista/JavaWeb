package logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.ContatoDao;
import model.Contato;

public class RemoveContatoLogic implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		long id = Long.parseLong(req.getParameter("id"));
		Contato contato = new Contato();
		contato.setId(id);
		Connection connection = (Connection) req.getAttribute("connection");
		new ContatoDao(connection).exclui(contato);
		System.out.println("Excluindo contato... ");
		return "mvc?logica=ListaContatosLogic";
	}

}
