package logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.ContatoDao;
import model.Contato;

public class ManipulaContatoLogic implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Contato contato;
		boolean possuiId = req.getParameterMap().containsKey("id");
		Connection connection = (Connection) req.getAttribute("connection");
		if (possuiId) {
			long id = Long.parseLong(req.getParameter("id"));
			contato = new ContatoDao(connection).getContato(id);
		} else {
			contato = new Contato();
			contato.setId(-1);
			contato.setDataNascimento("01/01/1990");
		}
		req.setAttribute("contato", contato);
		return "/WEB-INF/jsp/manipula-contato.jsp";
	}

}
