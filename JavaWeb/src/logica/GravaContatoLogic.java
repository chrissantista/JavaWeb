package logica;

import java.sql.Connection;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.ContatoDao;
import model.Contato;

public class GravaContatoLogic implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		long id = Long.parseLong(req.getParameter("id"));
		Contato contato = new Contato();
		contato.setId(id);
		contato.setNome(req.getParameter("nome"));
		contato.setEmail(req.getParameter("email"));
		contato.setEndereco(req.getParameter("endereco"));
		try {
			contato.setDataNascimento(req.getParameter("dataNascimento"));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		Connection connection = (Connection) req.getAttribute("connection");
		ContatoDao dao = new ContatoDao(connection);
		boolean eContatoNovo = id == -1;
		if (eContatoNovo)
			dao.adiciona(contato);
		else
			dao.altera(contato);
		return "mvc?logica=ListaContatosLogic";
	}

}
