package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Contato;

public class ContatoDao {
	private Connection connection;
	
	public ContatoDao(Connection connection) {
		this.connection = connection;
	}
	
	public void adiciona(Contato contato) {
		contato.setId(-1);
		String sql = "insert into contatos values (null,?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());
			stmt.setDate(4, contato.getDataNascimentoMysql());
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next())
				contato.setId(rs.getLong(1));
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean altera(Contato contato) {
		boolean retorno = false;
		String sql = "update contatos set "
				+ "nome=?, email=?, endereco=?, dataNascimento=? "
				+ "where id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());
			stmt.setDate(4, contato.getDataNascimentoMysql());
			stmt.setLong(5, contato.getId());
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next())
				contato.setId(rs.getLong(1));
			stmt.close();
			retorno = true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return retorno;
	}
	
	public boolean exclui(Contato contato) {
		boolean retorno = false;
		String sql = "delete from contatos where id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, contato.getId());
			stmt.execute();
			retorno = true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return retorno;
	}
	
	public Contato getContato(long id) {
		Contato contato = null;
		String sql = "Select * from contatos where id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				contato = new Contato();
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));
				contato.setDataNascimento(rs.getDate("dataNascimento"));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return contato;
	}
	
	public List<Contato> getLista() {
		List<Contato> contatos = new ArrayList<Contato>();
		String sql = "Select * from contatos";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Contato contato = new Contato();
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));
				contato.setDataNascimento(rs.getDate("dataNascimento"));
				contatos.add(contato);
			}
			return contatos;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
