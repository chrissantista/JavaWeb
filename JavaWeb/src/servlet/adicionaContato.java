package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbc.ContatoDao;
import model.Contato;

/**
 * Servlet implementation class adicionaContato
 */
@WebServlet("/adicionaContato")
public class adicionaContato extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		Contato contato = new Contato();
		contato.setNome(request.getParameter("nome"));
		contato.setEmail(request.getParameter("email"));
		contato.setEndereco(request.getParameter("endereco"));
		try {
			contato.setDataNascimento(request.getParameter("dataNascimento"));
		} catch (ParseException e) {
			out.println("Erro na convers�o da data.");
		}
		Connection connection = (Connection) request.getAttribute("connection");
		ContatoDao dao = new ContatoDao(connection);
		dao.adiciona(contato);
		request.setAttribute("id", contato.getId());
		RequestDispatcher rd = request.getRequestDispatcher("/contato-adicionado.jsp");
		rd.forward(request, response);
	}

}
