package model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Contato {
	private long id;
	private String nome;
	private String email;
	private String endereco;
	private Calendar dataNascimento;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public Calendar getDataNascimento() {
		return dataNascimento;
	}
	public Date getDataNascimentoMysql() {
		Date data = new Date(dataNascimento.getTimeInMillis()); 
		return data;
	}
	public String getDataNascimentoString() {
		java.util.Date dataDate = dataNascimento.getTime();
		String dataString = new SimpleDateFormat("dd/MM/yyyy").format(dataDate);
		return dataString;
	}
	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = Calendar.getInstance();
		this.dataNascimento.setTimeInMillis(dataNascimento.getTime());
	}
	public void setDataNascimento(String dataNascimento) throws ParseException {
		java.util.Date dataDate = new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento);
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataDate);
		this.dataNascimento = dataCalendar;
	}
}
